package pl.akademiakodu.hibernateZad1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HibernateZad1Application {

	public static void main(String[] args) {
		SpringApplication.run(HibernateZad1Application.class, args);
	}
}
