package pl.akademiakodu.hibernateZad1.controller;

import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pl.akademiakodu.hibernateZad1.dao.PersonRepository;
import pl.akademiakodu.hibernateZad1.model.Person;

import java.util.Optional;


@Controller
@RequestMapping("/person")
public class MainController {

    @Autowired
    private PersonRepository personRepository;


    @GetMapping("/")
    public String showMainPage(){
        return "index";
    }

    @GetMapping("/add")
    public String addPerson(){
        return "addperson";
    }

    @GetMapping("/findbysurname")
    public String findWithSurname(){
        return "findbysurname";
    }

    @GetMapping("/delete")
    public String deletePerson(ModelMap modelMap){
        modelMap.addAttribute("persons", personRepository.findAll());
        return "delete";
    }

    @GetMapping("/delete/{id}")
    public String deletePerson(@PathVariable Long id, ModelMap modelMap){
        modelMap.addAttribute("person", personRepository.findById(id));
        personRepository.deleteById(id);

        return "deletenotification";
    }

//    @PostMapping("/persondeleted")
//    public String deletedPerson(){
//
//        return "persondeleted";
//    }

    @PostMapping("/found")
    public String foundBySurname(@RequestParam String surname, ModelMap modelMap){
        modelMap.addAttribute("persons",personRepository.findBySurname(surname));
        return "found";
    }

    @PostMapping("/addnotification")
    public String addNotification(@ModelAttribute Person person, ModelMap modelMap){
        modelMap.put("person", person);
        personRepository.save(person);
        return "addnotification";
    }

    @GetMapping("/show")
    public String showPersons(ModelMap modelMap){
        modelMap.put("persons", personRepository.findAll());
        return "show";
    }
}
