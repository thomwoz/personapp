package pl.akademiakodu.hibernateZad1.dao;

import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.hibernateZad1.model.Person;

import java.util.List;

public interface PersonRepository extends CrudRepository<Person, Long> {
    List<Person> findBySurname(String surname);
}
